# donald-motswiri-module-3

M3-Assessment 1

## Instructions:
Create a Gitlab account
Create a repository on Gitlab as name-surname-module-3
Create 3 .dart files for the exercise. All code outputs are printed on the console.


### Assessment:
All the code must be submitted through a Gitlab repository link. 

Create a Flutter app that has the following screens, navigating to each other. Your app must have up to 6 screens (login, dashboard, 2 feature screens, and user profile edit):


1. Login and registration screens (not linked to a database) with relevant input fields.
1. Dashboard (the screen after login) with buttons to feature screens of your app.
1. The last screen must be of a user profile edit.
1. Dashboard Floating button on the home screen, linking to another screen.
1. Each screen must be labeled appropriately on the app bar.
